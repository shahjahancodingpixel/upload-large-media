import React, {useState} from 'react';
import {Alert, FlatList, Image, TouchableOpacity, View} from 'react-native';
import CustomImageCropPicker from '../../shared/components/customImageCropPicker';
import CustomLoading from '../../shared/components/customLoading';
import CustomText from '../../shared/components/customText';
import {RF, THEME, uploadMedia} from '../../shared/exporter';
import VideoPlayer from 'react-native-video-controls';
import styles from './styles';

const {white, black} = THEME.colors;

const Home = () => {
  const [showImagePicker, setShowImagePicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [media, setMedia] = useState<any>([]);

  const toggleImagePicker = () => {
    setShowImagePicker(!showImagePicker);
  };

  const uploadHandler = (media: any) => {
    setLoading(true);
    uploadMedia(media)
      .uploadProgress(() => {})
      .then(response => response.json())
      .then(res => {
        setMedia((prev: any) => [
          {isVideo: !!media.duration, source: res.secure_url},
          ...prev,
        ]);
      })
      .catch(err => {
        console.log('err', err);
      })
      .finally(() => setLoading(false));
  };

  return (
    <View style={styles.container}>
      <UploadBtn onPress={toggleImagePicker} />
      <FlatList
        data={media}
        keyExtractor={(_, index) => String(index)}
        renderItem={ItemCard}
      />
      <CustomImageCropPicker
        includeVideo
        visible={showImagePicker}
        toggleImagePicker={toggleImagePicker}
        getSource={uploadHandler}
      />
      <CustomLoading visible={loading} />
    </View>
  );
};

const ItemCard = ({item, index}: any) => {
  const {source, isVideo} = item;
  return (
    <>
      {isVideo ? (
        <VideoPlayer
          disableFullscreen
          disableBack
          disableVolume
          source={{
            uri: source,
          }}
          style={styles.sourceSize}
        />
      ) : (
        <Image source={{uri: source}} style={styles.sourceSize} />
      )}
    </>
  );
};

const UploadBtn = ({onPress}: any) => (
  <TouchableOpacity style={styles.uploadBtnContainer} onPress={onPress}>
    <CustomText bold color={white}>
      Select Media
    </CustomText>
  </TouchableOpacity>
);

export default Home;
