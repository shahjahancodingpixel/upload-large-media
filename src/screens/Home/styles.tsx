import {StyleSheet} from 'react-native';
import {GST, RF, THEME, WP} from '../../shared/exporter';

const {white, skyblue} = THEME.colors;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...GST.pt10,
    alignItems: 'center',
  },
  uploadBtnContainer: {
    backgroundColor: skyblue,
    borderRadius: RF(20),
    ...GST.py3,
    ...GST.px5,
    ...GST.mb5,
  },
  sourceSize: {
    width: WP(90),
    height: RF(200),
    ...GST.mb5,
  },
});

export default styles;
