import {Platform} from 'react-native';

const CLOUDINARY = {
  CLOUD_NAME: 'diversepro',
  PRESETS: {
    POST_MEDIA: 'post_media',
  },
  SLICE_SIZE: 20000000,
};

let HEADERS: any = {
  // 'Content-Type': 'multipart/form-data,octet-stream',
  'Content-Type': 'application/json',
};

const ANDROID = Platform.OS === 'android';
const IOS = Platform.OS === 'ios';

export {HEADERS, CLOUDINARY, ANDROID, IOS};
