import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {GST, THEME} from '../../exporter';

const {primary} = THEME.colors;

const CustomLoading = ({
  visible,
}: {
  visible: boolean;
  showProgress?: boolean;
  progress?: number;
}) => {
  return (
    <>
      {visible && (
        <View style={styles.container}>
          <ActivityIndicator size={'large'} color={primary} />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    ...StyleSheet.absoluteFillObject,
  },
});

export default CustomLoading;
