import React from 'react';
import {Text, TextProps} from 'react-native';
import {RF, THEME} from '../../exporter';

interface Props extends TextProps {
  style: any;
  bold: boolean;
  semiBold: boolean;
  size: number;
  color: string;
  capital: boolean;
  children: any;
  numberOfLines: number;
  italic: boolean;
  onPress: () => void;
}

const CustomText = (props: Partial<Props>) => {
  const {
    size = 12,
    color = THEME.colors.black,
    style,
    numberOfLines = 0,
    capital = false,
    onPress,
  } = props;
  return (
    <Text
      onPress={onPress}
      numberOfLines={numberOfLines}
      style={[
        {
          fontWeight: props.bold ? 'bold' : 'normal',
          fontSize: RF(size),
          color,
          textTransform: capital ? 'uppercase' : 'none',
        },
        style,
      ]}>
      {props.children}
    </Text>
  );
};

export default CustomText;
