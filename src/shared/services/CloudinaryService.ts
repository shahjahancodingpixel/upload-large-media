import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';
import {CLOUDINARY, HEADERS, IOS} from '../exporter';

export const uploadMedia = (data: any) => {
  const contentType = data.duration ? 'video' : 'image';
  let array = data.path.split('.');
  let fileEnding = array[array.length - 1].toLowerCase();
  fileEnding = fileEnding || 'jpg';
  const realPath = IOS ? data.path.replace('file://', '') : data.path;

  const params = [
    {name: 'upload_preset', data: CLOUDINARY.PRESETS.POST_MEDIA},
    {name: 'cloud_name', data: CLOUDINARY.CLOUD_NAME},
    {
      name: 'file',
      filename: data.filename || 'file.png',
      type: `${contentType}/${fileEnding}`,
      data: RNFetchBlob.wrap(decodeURIComponent(realPath)),
    },
  ];
  return RNFetchBlob.fetch(
    'POST',
    `https://api.cloudinary.com/v1_1/${CLOUDINARY.CLOUD_NAME}/${contentType}/upload`,
    HEADERS,
    params,
  );
};

export const uploadLargeMedia = async (file: any, preset: string) => {
  // const file = e.target.files[0];

  const POST_URL =
    'https://api.cloudinary.com/v1_1/' + CLOUDINARY.CLOUD_NAME + '/auto/upload';

  const XUniqueUploadId: any = +new Date();

  const noop = () => {};

  const slice = (file: any, start: any, end: any) => {
    const slice = file.mozSlice
      ? file.mozSlice
      : file.webkitSlice
      ? file.webkitSlice
      : file.slice
      ? file.slice
      : noop;

    return slice.bind(file)(start, end);
  };

  const processFile = () => {
    const size = file.size;
    const sliceSize = CLOUDINARY.SLICE_SIZE;
    let start = 0;

    const loop = () => {
      let end = start + sliceSize;

      if (end > size) {
        end = size;
      }
      const s = slice(file, start, end);
      send(s, start, end - 1, size);
      if (end < size) {
        start += sliceSize;
        setTimeout(loop, 3);
      }
    };

    setTimeout(loop, 3);
  };

  processFile();

  const send = (piece: any, start: any, end: any, size: any) => {
    const formdata = new FormData();
    formdata.append('file', piece);
    formdata.append('cloud_name', CLOUDINARY.CLOUD_NAME);
    formdata.append('upload_preset', preset);

    try {
      // make axios post request
      const response = axios({
        method: 'post',
        url: POST_URL,
        data: formdata,
        headers: {
          'Content-Type': 'multipart/form-data',
          'X-Unique-Upload-Id': XUniqueUploadId,
          'Content-Range': 'bytes ' + start + '-' + end + '/' + size,
        },
      });
      // console.log('response', response);
    } catch (error) {
      // console.log('error', error);
    }

    //   const xhr = new XMLHttpRequest();
    //   xhr.open('POST', POST_URL, true);
    //   xhr.setRequestHeader('X-Unique-Upload-Id', XUniqueUploadId);
    //   xhr.setRequestHeader(
    //     'Content-Range',
    //     'bytes ' + start + '-' + end + '/' + size,
    //   );

    //   xhr.onload = () => {
    //     // do something to response
    //     console.log();
    //   };

    //   xhr.send(formdata);
    // };
  };
};
