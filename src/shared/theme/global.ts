import {StyleSheet} from 'react-native';
import {spacing} from './spacing';

export const GST = StyleSheet.create({
  ...spacing,
});
