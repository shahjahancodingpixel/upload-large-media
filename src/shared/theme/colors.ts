export const THEME = {
  colors: {
    primary: '#1EAFBD',
    white: '#FFFFFF',
    black: '#000000',
    lightGrey: '#E9E9E9',
    orange: '#F04B22',
    skyblue: '#64B5F3',
    yellow: '#FFC56D',
  },
};
